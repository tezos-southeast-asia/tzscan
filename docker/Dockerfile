# docker build -t tzscan .
# The source directory tzscan/ must exist at the directory with Dockerfile

FROM ubuntu:18.04
MAINTAINER Jun FURUSE <jun.furuse@dailambda.jp>

# Files are downloaded from the following mirrors.
ENV OPAM_REPO=git+https://gitlab.com/dailambda/opam-repository-tezos
ENV ARCHIVE=https://gitlab.com/dailambda/opam-repository-tezos/raw/master/archive

ENV DEBIAN_FRONTEND=noninteractive
ENV apt_install="apt install -y --no-install-recommends"

# CWD is /.  Let's do inside /root
ENV HOME=/root

#
# apt install
#

RUN apt clean && apt update && apt upgrade -y
RUN ${apt_install} curl \
  git \
  ca-certificates \
  make \
  patch \
  sudo \
  ocaml \
  g++ \
  pkg-config \
  libgmp-dev \
  libev-dev \
  libhidapi-dev \
  m4 \
  unzip \
  bubblewrap \
  postgresql \
  libsodium-dev \
  libgeoip1 \
  geoip-database \
  libcurl4-gnutls-dev \
  zlib1g-dev \
  libgeoip-dev \
  tzdata \
  rsync

#
# OCaml 4.07.1 and OPAM 2.0.3
#

WORKDIR ${HOME}

RUN git clone https://github.com/ocaml/opam
WORKDIR ${HOME}/opam
RUN git checkout 2.0.3
RUN ./configure
RUN make lib-ext && make && make install
RUN opam init -a --bare --disable-sandboxing # bwrap does not work for me.
RUN opam switch create 4.07.1
# We compiled our own OCaml 4.07.1.  Let's uninstall those from apt.
RUN apt remove -y ocaml ocaml-base ocaml-base-nox ocaml-compiler-libs ocaml-interp ocaml-nox

#
# We only use files under the mirror
#

# We use a mirror with copied source archives.
RUN opam repo add tzscan ${OPAM_REPO}
RUN opam repo remove default

#
# Tzscan
#

ADD tzscan ${HOME}/tzscan

# Instead of git submodule as documented, we download files from the mirro

WORKDIR ${HOME}/tzscan/libs

RUN curl -O ${ARCHIVE}/amcharts3.dev.tgz && tar xf amcharts3.dev.tgz
RUN curl -O ${ARCHIVE}/ammap3.dev.tgz && tar xf ammap3.dev.tgz
RUN curl -O ${ARCHIVE}/ez-api.dev.tgz && tar xf ez-api.dev.tgz
RUN curl -O ${ARCHIVE}/ez-pgocaml.dev.tgz && tar xf ez-pgocaml.dev.tgz
RUN curl -O ${ARCHIVE}/ocplib-json-typed.428b9b4ac47a79810b65c4aef961d45e6688d13a.tgz && tar xf ocplib-json-typed.428b9b4ac47a79810b65c4aef961d45e6688d13a.tgz
RUN curl -O ${ARCHIVE}/ocplib-jsutils.dev.tgz && tar xf ocplib-jsutils.dev.tgz
RUN curl -O ${ARCHIVE}/ocplib-jsutils.dev.tgz && tar xf ocplib-jsutils.dev.tgz
RUN curl -O ${ARCHIVE}/pgocaml.dev.tgz && tar xf pgocaml.dev.tgz

#
# Postgresql init
#

# Using user 'root'
WORKDIR /
USER postgres
RUN /etc/init.d/postgresql start \
   && psql --command "CREATE USER root; ALTER ROLE root CREATEDB"
USER root

#
# The build
#

WORKDIR ${HOME}/tzscan

RUN /etc/init.d/postgresql start \
   && eval $(opam env) \
   && opam install -y libs/pgocaml .
