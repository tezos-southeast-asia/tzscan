(* ez-api/libs/ez-api/gMTime.ml is a good example of 
   buggy handling of UTC and local time zones.

   This module replaces the functions.
 *)

open CalendarLib

let nsecs_per_day = 24 * 60 * 60

type t = UTC of Unix.tm

let utc_of_epoch t =
  let local = Calendar.from_unixfloat t in
  UTC (Calendar.(to_unixtm @@ to_gmt local))

let epoch_of_utc
      (UTC { Unix.tm_sec;   (* Seconds 0..60 *)
             tm_min;   (* Minutes 0..59 *)
             tm_hour;  (* Hours 0..23 *)
             tm_mday;  (* Day of month 1..31 *)
             tm_mon;   (* Month of year 0..11 *)
             tm_year;  (* Year - 1900 *)
             tm_isdst; (* Daylight time savings in effect *)
             _ }) =
  assert (tm_isdst = false);
  let utc = Calendar.make (tm_year + 1900) (tm_mon + 1) tm_mday tm_hour tm_min tm_sec in
  let local = Calendar.from_gmt utc in
  Calendar.to_unixfloat local
  
let date_of_utc (UTC tm) =
  Printf.sprintf "%04d-%02d-%02dT%02d:%02d:%02dZ"
    (tm.Unix.tm_year+1900)
    (tm.Unix.tm_mon+1)
    tm.Unix.tm_mday
    tm.Unix.tm_hour
    tm.Unix.tm_min
    tm.Unix.tm_sec

let utc_of_date date =
  let date = Tezos_types.Date.to_string date in
  let datelen = String.length date in
  assert (datelen >= 19);
  assert (date.[4] = '-');
  assert (date.[7] = '-');
  assert (date.[10] = 'T');
  assert (date.[13] = ':');
  assert (date.[16] = ':');
  assert (date.[19] = 'Z');
  let tm_year = int_of_string (String.sub date 0 4) - 1900 in
  let tm_mon = int_of_string (String.sub date 5 2) - 1 in
  let tm_mday = int_of_string (String.sub date 8 2) in
  let tm_hour = int_of_string (String.sub date 11 2) in
  let tm_min = int_of_string (String.sub date 14 2) in
  let tm_sec = int_of_string (String.sub date 17 2) in
  UTC (Unix.({
           tm_year; tm_mon; tm_mday;
           tm_hour; tm_min; tm_sec;
           (* recomputed *)
           tm_wday = 0; tm_yday = 0; tm_isdst = false;
  }))
    
let epoch = Unix.gettimeofday


  
